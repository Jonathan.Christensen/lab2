package INF101.lab2.pokemon;

import java.lang.Math;
import java.lang.annotation.Target;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;

    public Pokemon(String name){
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));


    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
        }

    @Override
    public int getCurrentHP() {
        return healthPoints;
        }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        return healthPoints > 0;
    }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);

        if (target.isAlive()){
            System.out.println(this.name + " attacks " + target.getName());
        }
        else{
        System.out.println(target.getName() + " is defeated by " + this.name);
        }
        

    }

    @Override
    public void damage(int damageTaken) {
        healthPoints = healthPoints - damageTaken;
        if (healthPoints < 0){
            healthPoints = 0;
        }
        if (healthPoints > maxHealthPoints){
            healthPoints = maxHealthPoints;
        }
        if (damageTaken <= 0){
            damageTaken = 0;

        }
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");

    }

    @Override
    public String toString() {
        return this.name + " HP: (" + this.healthPoints + "/" + this.maxHealthPoints + ") STR: " + this.strength;
    }

}
